var myApp = angular.module('myApp',[]);

myApp.controller("ctrl1", function($scope, $http){

	// Scope variables
  $scope.servers = [
    {
      Name: "srv-web-01",
      Uptime: 125,
      OS: "Debian"
    },
    {
      Name: "srv-web-02",
      Uptime: 10,
      OS: "Debian"
    },
    {
      Name: "srv-web-03",
      Uptime: 406,
      OS: "CentOS"
    },
    {
      Name: "srv-ldap-01",
      Uptime: 234,
      OS: "RedHat"
    },
    {
      Name: "srv-db-01",
      Uptime: 12,
      OS: "Solaris"
    },
    {
      Name: "srv-db-02",
      Uptime: 45,
      OS: "Debian"
    },
  ];
});
