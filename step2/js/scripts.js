var myApp = angular.module('myApp',[]);

myApp.controller("ctrl1", function($scope, $http){

	// Scope variables
  $scope.headers = [];
  $scope.servers = [];
  
  // Main
  $http.get("data/servers.csv").then(function(res){
      var lines = res.data.split('\n');
      lines.forEach(function(line, lineIndex) {
        if ( lineIndex == 0 ) {
          // This line contains headers
          $scope.headers = line.split(',');
        }
        else {
          // This line contains a server data
          var server = {};
          var fields = line.split(',');
          // Make server there is as many fields as headers
          if ( fields.length == $scope.headers.length ) {
            fields.forEach(function(field, fieldIndex) {
              server[$scope.headers[fieldIndex]] = field;
            });
            $scope.servers.push( server );
          }
        }
      });
    });
});
